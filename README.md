### Hi there 👋
<br>

- 👨‍🎓 I'm a Computer Science student - 8th semester (some repos are currently private).
- 💻 Now I'm focusing on studies and projects concerning both front and back-end area.

<br>

<div align="center">
    <h3>👤 My React.js <a href="https://eduardopontes.netlify.app/">portifolio page</a>.</h3>
</div>

<br>

<div align="center">
    <a href="https://github.com/eduardolpsss">
        <img height="140em"
            src="https://github-readme-stats.vercel.app/api?username=eduardolpsss&show_icons=true&theme=radical&include_all_commits=true&count_private=true" />
        <img height="140em"
            src="https://github-readme-stats.vercel.app/api/top-langs/?username=eduardolpsss&layout=compact&langs_count=8&theme=radical" />
    </a>
</div>

<br>

<div align="center"><br>
    <img align="center" alt="Rafa-Js" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-plain.svg">
        <img align="center" alt="Rafa-Node" height="30" width="40"
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/nodejs/nodejs-plain.svg">
        <img align="center" alt="Rafa-Node" height="30" width="40"
            src="https://raw.githubusercontent.com/devicons/devicon/master/icons/typescript/typescript-plain.svg">
        <img align="center" alt="Rafa-HTML" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original.svg">
    <img align="center" alt="Rafa-CSS" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original.svg">
    <img align="center" alt="Rafa-React" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/react/react-original.svg">
    <img align="center" alt="Rafa-Java" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg">
    <img align="center" alt="Rafa-C" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/c/c-original.svg">
    <img align="center" alt="Rafa-C" height="30" width="40"
        src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original.svg">
</div>

<br><br>

<div align="center">
    <a href="https://www.linkedin.com/in/eduardolpsss/" target="_blank"><img
            src="https://img.shields.io/badge/-LinkedIn-%230077B5?style=for-the-badge&logo=linkedin&logoColor=white"
            target="_blank"></a>
    <a href="mailto:eduardo.pontes2801@gmail.com"><img
            src="https://img.shields.io/badge/-Gmail-%23333?style=for-the-badge&logo=gmail&logoColor=white"
            target="_blank"></a>
</div>
